using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    float walkSpeed ,runSpeed;
    float moveSpeed;
    float rotSpeed;
    public Camera cam;
    Animator animator;
    public GameObject cameraArm;
    float hAxis, vAxis;
    bool isRunning;
    Vector3 moveVec;

    private float currentCameraRotationX;
    public float interactDiastance = 0.2f;

    void Start()
    {
        walkSpeed = 3.0f;
        runSpeed = 6.0f;
        moveSpeed = 5.0f;
        rotSpeed = 3.0f;
        animator = GetComponentInChildren<Animator>();
    }
    
    void Update()
    {
        MoveController();
        RotController();

        if (Input.GetKeyDown(KeyCode.E))
        {
            DoorOpen();
        }
    }

    void MoveController()
    {

        if (Input.GetKeyDown(KeyCode.LeftShift))
            isRunning = true;
        if (Input.GetKeyUp(KeyCode.LeftShift))
            isRunning = false;
        hAxis = Input.GetAxisRaw("Horizontal");
        vAxis = Input.GetAxisRaw("Vertical");

        moveVec = new Vector3(hAxis, 0, vAxis).normalized;

        if (isRunning)
            transform.Translate(moveVec * runSpeed * Time.deltaTime);
        else
            transform.Translate(moveVec * walkSpeed * Time.deltaTime);

        //이동 중일 경우 애니메이션을 위한 isWalk를 true로
        if (isRunning)
        {
            animator.SetBool("isRun", moveVec != Vector3.zero);
            animator.SetBool("isWalk", false);
        }
        else
        {
            animator.SetBool("isWalk", moveVec != Vector3.zero);
            animator.SetBool("isRun", false);
        }

    }

    void RotController()
    {
        float rotX = Input.GetAxis("Mouse Y") * rotSpeed;
        float rotY = Input.GetAxis("Mouse X") * rotSpeed;

        currentCameraRotationX -= rotX;
        currentCameraRotationX = Mathf.Clamp(currentCameraRotationX, -80, 50);

        this.transform.localRotation *= Quaternion.Euler(0, rotY, 0);
        cam.transform.localEulerAngles = new Vector3(currentCameraRotationX, 0, 0);
    }

    void DoorOpen()
    {
        Ray ray = new Ray(cameraArm.transform.position, cameraArm.transform.forward);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, interactDiastance))
        {
            Debug.DrawRay(ray.origin, ray.direction * 10f, Color.red, 5f);
            if (hit.collider.CompareTag("Door"))
            {
            hit.transform.gameObject.GetComponent<DoorController>().ChangeDoorState();
            Debug.Log("문열기 코드");
            }
        }
    }
    
}
