using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class ZombieController : MonoBehaviour
{
    public LayerMask targetLayer;
    public NavMeshAgent nav;
    public Animator animator;
    public GameObject player;
    public BoxCollider attackCollider;
    private float distance;
    public float hAxis, vAxis;
    Vector3 moveVec;
    public float sight = 10f;

    public float damage = 10f;
    public float timeBetAttack = 0.5f;
    void Start()
    {
        nav = GetComponent<NavMeshAgent>();
        nav.speed = 1f;
        animator = GetComponent<Animator>();
        attackCollider = GetComponent<BoxCollider>();
    }
    
    void Update()
    {
        if (player == null)
            return;
        distance = Vector3.Distance(player.transform.position, transform.position);
        if (distance <= sight)
        {
            nav.SetDestination(player.transform.position);
        }
        animator.SetBool("isWalk", nav.remainingDistance != 0);

    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag == "Player")
            animator.SetBool("isAttack", true);
    }
}
